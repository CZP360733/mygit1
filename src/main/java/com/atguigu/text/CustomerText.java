package com.atguigu.text;


import com.atguigu.dao.CustomerDao;
import com.atguigu.domain.Customer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext.xml")
public class CustomerText {
    @Autowired
    private CustomerDao customerDao;
    @Test
    public void text1(){
        Customer customer = new Customer();
        customer.setCustName("陈智平");
        customer.setCustSource("java高级工程师");
        customer.setCustIndustry("软件开发");
        customer.setCustLevel("超级管理员");
        customer.setCustAddress("阿里大厦");
        customer.setCustPhone("15219140612");
        customerDao.save(customer);
    }

    @Test
    public void text2(){
        Customer one = customerDao.findOne(2l);
        System.out.println(one);
    }


}
